"use strict";

/* 1. Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM):
Это HTML обработанный в браузере, для отрисовки страницы. Через JS можем менять его код напрямую, для изменения страницы */

function arrToList(arr, dom = document.body) {
  const ol = document.createElement("ol");
  arr.forEach(function callback(value) {
    const listItem = document.createElement("li");
    if (Array.isArray(value)) {
      arrToList(value, listItem);
    } else {
      listItem.textContent = value;
    }
    ol.append(listItem);
    dom.append(ol);
  });
}

const arrIn = [
  "Kharkiv",
  "Kiev",
  ["Borispol", "Irpin"],
  "Odessa",
  "Lviv",
  "Dnieper",
];

const p = document.createElement("p");
p.id = "demo";
p.style.cssText = "font-size: 60px; margin-left: 50px";

document.body.append(p);

let countDownDate = new Date().getTime() + 10000; // очищаю через 10 секунд, глючит в браузере время

let x = setInterval(function () {
  let now = new Date().getTime();
  let distance = countDownDate - now;

  let seconds = Math.floor((distance % (1000 * 60)) / 1000);

  document.getElementById("demo").innerHTML = seconds + "s ";

  if (distance < 0) {
    clearInterval(x);
    document.body.innerHTML = "";
  }
}, 1000);

arrToList(arrIn);
